﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScriptPlayPause : MonoBehaviour
{
    public Button btnPlay, btnPause;


    ScriptKarakter _scriptKarakter;
    ScriptBackground2 _scriptBackground2;
    ScriptEnemy _scriptEnemy;
    ScriptSpeed _scriptSpeed;   

    private void Awake()
    {
        _scriptSpeed = GameObject.FindObjectOfType<ScriptSpeed>();
        _scriptKarakter = GameObject.FindObjectOfType<ScriptKarakter>();
        _scriptBackground2 = GameObject.FindObjectOfType<ScriptBackground2>();
        _scriptEnemy = GameObject.FindObjectOfType<ScriptEnemy>();
    }
    void Start()
    {
        btnPlay.onClick.AddListener(FonkBtnPlay);
        btnPause.onClick.AddListener(FonkBtnPause);

        btnPause.gameObject.SetActive(false);
        btnPlay.gameObject.SetActive(false);
    }

  public void FonkBtnPlay() {
        btnPause.gameObject.SetActive(true);
        btnPlay.gameObject.SetActive(false);
        Time.timeScale = 1;
       /* LeanTween.resumeAll();
        _scriptKarakter.FonkSetSpeedPlayer(-400);
        _scriptBackground2.FonkSetSpeedBg(4);
        _scriptEnemy.FonkSetNewSpeed(4);
        _scriptSpeed.FonkSetTouch(true);*/
    }

    void FonkBtnPause() { //Durdur
        btnPause.gameObject.SetActive(false);
        btnPlay.gameObject.SetActive(true);
        Time.timeScale = 0;
       /* LeanTween.pauseAll();
        _scriptSpeed.FonkSetTouch(false);
        _scriptKarakter.FonkSetSpeedPlayer(0);
        _scriptBackground2.FonkSetSpeedBg(0);
        _scriptEnemy.FonkSetNewSpeed(0);*/

    }
}
