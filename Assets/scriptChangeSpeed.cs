﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptChangeSpeed : MonoBehaviour
{
    ScriptKarakter _scriptKarakter;
    ScriptBackground2 _scriptBackground2;
    ScriptEnemy _scriptEnemy;
    ScriptSpeed _scriptSpeed;
    private void Awake()
    {
        _scriptSpeed = GameObject.FindObjectOfType<ScriptSpeed>();
        _scriptKarakter = GameObject.FindObjectOfType<ScriptKarakter>();
        _scriptBackground2 = GameObject.FindObjectOfType<ScriptBackground2>();
        _scriptEnemy = GameObject.FindObjectOfType<ScriptEnemy>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator IncSpeed()
    {
        float counter = 1;

        while (counter<4.1f)
        {
            yield return new WaitForSeconds(.1f);
            _scriptKarakter.FonkSetSpeedPlayer(-100*counter);
            _scriptBackground2.FonkSetSpeedBg(counter);
            _scriptEnemy.FonkSetNewSpeed(counter);
            counter += .2f;
        }
        
    }
}
