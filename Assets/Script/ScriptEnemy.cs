﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptEnemy : MonoBehaviour
{
    public GameObject EnemyA, EnemyB, EnemyC;
    public float speedEnemy;

    void Start()
    {
        speedEnemy = 4;
    }



    private void Update()
    {

        EnemyA.transform.Translate(-speedEnemy * Time.deltaTime, 0, 0);
        EnemyB.transform.Translate(-speedEnemy * Time.deltaTime, 0, 0);
        EnemyC.transform.Translate(-speedEnemy * Time.deltaTime, 0, 0);


        if (EnemyA.transform.localPosition.x < -4.55f) {
            FonkSetNewPosition(EnemyA);


        }
        if (EnemyB.transform.localPosition.x < -4.55f) {
            FonkSetNewPosition(EnemyB);

        }
        if (EnemyC.transform.localPosition.x < -4.55f) {
            FonkSetNewPosition(EnemyC);

        }
    }




    void FonkSetNewPosition(GameObject myObj)
    {
        myObj.transform.localPosition = new Vector3(myObj.transform.localPosition.x+12.6f,
                                                    myObj.transform.localPosition.y,
                                                    0);
    }

    public void FonkSetNewSpeed(float newSpeed)
    {
        speedEnemy = newSpeed;
    }
}
