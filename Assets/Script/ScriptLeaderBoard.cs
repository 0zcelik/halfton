﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using UnityEngine.UI;
using UnityEngine.Networking;
public class ScriptLeaderBoard : MonoBehaviour
{
    JsonData jsonParser;
    public Text scoreText, userNumber;
    public GameObject scoreTable;
    public static ScriptLeaderBoard instance;
    List<UserGetSet> userList = new List<UserGetSet>();
    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        // scoreTable = GameObject.Find("scoreTable");
        scoreTable.SetActive(false);
        StartCoroutine(getUserData());

    }

    public void scoreTableEnable()
    {
        //leaderBoardStart();
        scoreText.text = null;
        scoreTable.SetActive(true);
        //Invoke("invokeData", 0.5f);
        StartCoroutine(getUserData());

    }

    public void invokeData()
    {
        userCount();
        for (int i = 0; i < 3; i++)
        {
            scoreText.text += (i + 1) + " - " + userList[i].getKulAd().ToString() + "  " + userList[i].getKulPuan().ToString() + "\n";
        }

    }


    public void scoreTableDisable()
    {
        Debug.Log("scoreTableDisable");

        userList.Clear();

        scoreTable.SetActive(false);
    }

    public void leaderBoardStart()
    {
        Debug.Log("leaderBoardStart");
        //StartCoroutine(getUserData());
    }

    public void userCount()
    {
        int i = 0;
        for (i = 0; i < userList.Count; i++)
        {
            if (userList[i].getKulAd().Equals(PlayerPrefs.GetString("userName")))
            {
                break;
            }
        }
        userNumber.text = "Your Ranking " + (i + 1).ToString();
    }

    IEnumerator getUserData()
    {
        userList.Clear();
        WWWForm fakeRequest = new WWWForm();
        fakeRequest.AddField("", "");
        WWW getData = new WWW("http://ibrahimozcelik.net/Halfton/getData.php", fakeRequest);
        yield return getData;
        Debug.Log(getData.text);
        jsonParser = JsonMapper.ToObject(getData.text);
        for (int i = 0; i < jsonParser["users"].Count; i++)
        {
            userList.Add(new UserGetSet(jsonParser["users"][i]["kulAd"].ToString(), jsonParser["users"][i]["kulSifre"].ToString(),
            int.Parse(jsonParser["users"][i]["kulPuan"].ToString())));
        }
        invokeData();
    }

    /*IEnumerator getUserData()
    {
        UnityWebRequest unityWebRequest = UnityWebRequest.Get("http://ibrahimozcelik.net/Halfton/getData.php");
        

        userList.Clear();
        yield return unityWebRequest.Send();
        Debug.Log(unityWebRequest.downloadHandler.text);
        GameObject.Find("demoTxt").GetComponent<Text>().text = unityWebRequest.downloadHandler.text;
    }*/
}
