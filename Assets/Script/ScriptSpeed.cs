﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptSpeed : MonoBehaviour
{
    public AudioSource touchSound;
    ScriptBackground2 _scriptBackground;
    ScriptKarakter _scriptKarakter;
    ScriptEnemy _scriptEnemy;
    bool touchControl;
    public GameObject btnPlay, introVideo;
    bool enterOnlyOnce,b_TouchOn,b_pause,entryOnce;

    private void Awake()
    {
        entryOnce = true;
        _scriptBackground = GameObject.FindObjectOfType<ScriptBackground2>();
        _scriptKarakter = GameObject.FindObjectOfType<ScriptKarakter>();
        _scriptEnemy = GameObject.FindObjectOfType<ScriptEnemy>();
    }

    private void Start()
    {
        enterOnlyOnce = false;
        b_TouchOn = true;
        touchControl = true;

    }
    int touchCounter;
    void Update()
    {
        if (!btnPlay.activeInHierarchy && b_TouchOn)
        {
            //if (!b_pause)
            //{
                if (Input.touchCount == 1)
                {
                if(touchControl)
                {
                    touchSound.Play();
                    touchControl = false;
                }

                if(touchCounter < 2 && entryOnce)
                {
                    touchCounter++;
                    entryOnce = false;
                    
                }

                if(touchCounter == 2)
                {
                    ScriptIntro.instance.deleteTouch();
                    touchCounter = 0;
                }

                _scriptBackground.FonkSetSpeedBg(2);
                    _scriptKarakter.FonkSetSpeedPlayer(-178);
                    _scriptEnemy.FonkSetNewSpeed(2);
                    enterOnlyOnce = true;
                }
                else if (Input.touchCount == 0)
                {
                touchControl = true;

                if (enterOnlyOnce)
                    {
                    entryOnce = true;
                    _scriptBackground.FonkSetSpeedBg(4);
                        _scriptKarakter.FonkSetSpeedPlayer(-400);
                        _scriptEnemy.FonkSetNewSpeed(4);
                        enterOnlyOnce = false;
                    }
                }
            //}
        }
    }

    public void FonkSetTouch(bool touchValue)
    {
        b_TouchOn = touchValue;
    }
}
