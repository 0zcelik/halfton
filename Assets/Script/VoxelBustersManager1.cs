﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

using VoxelBusters;
using VoxelBusters.NativePlugins;

public class VoxelBustersManager1 : MonoBehaviour
{
    private bool isSharing = false;

    public void ShareSocialMedia()
    {
        isSharing = true;
    }

    private void LateUpdate()
    {
        if (isSharing == true)
        {
            isSharing = false;
            StartCoroutine(CaptureScreenShoot());
        }
    }

    IEnumerator CaptureScreenShoot()
    {
        yield return new WaitForEndOfFrame();
        Debug.Log("CaptureScreenShoot");
        Texture2D texture = ScreenCapture.CaptureScreenshotAsTexture();
        ShareSheet(texture);
        Object.Destroy(texture);
    }

    private void ShareSheet(Texture2D texture)
    {
        ShareSheet _shareSheet = new ShareSheet();
        Debug.Log("ShareSheet");
        _shareSheet.Text = "Halfton \n\nAndroid Download: \n" + "https://play.google.com/store/apps/details?id=com.gngrfrdn.Halfton&gl=TR \n\n" + "IOS Download: \n"+ "https://apps.apple.com/tr/app/halfton/id1487830716?l=tr";
        _shareSheet.AttachImage(texture);
        //_shareSheet.URL = "https://play.google.com/store/apps/details?id=com.gngrfrdn.Halfton&gl=TR";

        //+"\nIOS: " + "https://apps.apple.com/tr/app/halfton/id1487830716?l=tr"

        NPBinding.Sharing.ShowView(_shareSheet,FinishSharing);

    }

    private void FinishSharing(eShareResult _result)
    {
        Debug.Log(_result);
    }
}
