﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptBackground2 : MonoBehaviour
{
    public GameObject BackA, BackB, BackC;
    public float speed;

    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        speed = 4;        
    }

    // Update is called once per frame
    void Update()
    {
        BackA.transform.Translate(-speed*Time.deltaTime,0,0);
        BackB.transform.Translate(-speed*Time.deltaTime,0,0);
        BackC.transform.Translate(-speed*Time.deltaTime,0,0);

        if (BackA.transform.localPosition.x< -8.24f){ FonkSetNewPossition(BackA); }
        if (BackB.transform.localPosition.x< -8.24f) { FonkSetNewPossition(BackB); }
        if (BackC.transform.localPosition.x< -8.24f) { FonkSetNewPossition(BackC); }
    }
    void FonkSetNewPossition(GameObject myObj)
    {
        myObj.transform.localPosition =new Vector3(myObj.transform.localPosition.x+17.82f, 0,0);
    }

    public void FonkSetSpeedBg(float newSpeed)
    {
        speed = newSpeed;
    }
}
