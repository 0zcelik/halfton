﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ScriptUpdateScore : MonoBehaviour
{
    //public TextMeshProUGUI lastScore;
    public static ScriptUpdateScore instance;

    private void Awake()
    {
        instance = this;
    }

    public void updateScore()
    {
        Debug.Log("updateScore");

        //PlayerPrefs.SetInt("BestScore", int.Parse(lastScore.text));
        startUpdate();
    }
    
    public void startUpdate()
    {
        Debug.Log("startUpdate");
        StartCoroutine(updateData());
    }

    IEnumerator updateData()
    {

        WWWForm request = new WWWForm();
        request.AddField("userName", PlayerPrefs.GetString("userName"));
        request.AddField("userScore", PlayerPrefs.GetInt("BestScore"));

        Debug.Log("startUpdate 2");

        WWW getData = new WWW("http://ibrahimozcelik.net/Halfton/updateScore.php", request);
        Debug.Log("UPDATE SCORE" + PlayerPrefs.GetInt("BestScore"));
        yield return getData;
    }
}
