﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptReward : MonoBehaviour
{

    public static ScriptReward instance;
    public string rewardType;
    private void Awake()
    {
        instance = this;
    }

    public void selectFace(string faceName)
    {
        Debug.Log(faceName);
        if (faceName.Equals("resume"))
        {
            rewardType = "resume";
        }
        else
        {
            rewardType = faceName;    
        }
           
        FindObjectOfType<VG_GoogleAdmob>().ShowRewardBasedVideo();

    }
}
