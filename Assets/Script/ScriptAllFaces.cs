﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[System.Serializable]
public class AllFace
{
    public string name;
    public Sprite sprite;
    public TextMeshProUGUI timer;
}



public class ScriptAllFaces : MonoBehaviour
{
    public GameObject player;
    public Sprite defaultFace;
    public List<AllFace> faces;
    public static ScriptAllFaces instance;
    public int timeCount;
    public TextMeshProUGUI timer;
    int min, sec;
    int timerCounter;

    private void Start()
    {
        timerCounter = 1800;
    }
    private void Awake()
    {
        instance = this;
    }

    public void startTimeFace()
    {
        StartCoroutine(startTime());
    }


    public void setTextMesh(TextMeshProUGUI timer)
    {
        this.timer = timer;
    }

    IEnumerator startTime()
    {
        while(timerCounter>0)
        {
            yield return new WaitForSeconds(1);

            timerCounter -= 1;
            min = timerCounter / 60;
            sec = timerCounter - (min * 60);
            this.timer.text = (min +"M:"+sec+"S");    
        }
        player.GetComponent<SpriteRenderer>().sprite = defaultFace;
    }


}
