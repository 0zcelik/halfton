﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class ScriptIntro : MonoBehaviour
{
    public GameObject EnemyA, EnemyB, EnemyC;
    public Button btnStart;
    public GameObject VideoIndex,GameScene,CodeManagers;
    public Image imgScore;
    VideoPlayer VPIndex;
    bool checkIndexVid;
    public GameObject touchBut,touchPanel;

    ScriptEnemy _scriptEnemy;
    ScriptBackground2 _scriptBackground2;
    ScriptKarakter _scriptKarakter;
    ScriptPlayPause _scriptPlayPause;
    scriptChangeSpeed _scriptChangeSpeed;
    public static ScriptIntro instance;
    Vector3 defaultTouchTextScale = new Vector3(1.974093f, 1.974093f, 1.974093f);
    bool enterOnlyOnce;
    private void Awake()
    {
        instance = this;
        _scriptEnemy = GameObject.FindObjectOfType<ScriptEnemy>();
        _scriptBackground2 = GameObject.FindObjectOfType<ScriptBackground2>();
        _scriptKarakter = GameObject.FindObjectOfType<ScriptKarakter>();
        _scriptPlayPause = GameObject.FindObjectOfType<ScriptPlayPause>();
        _scriptChangeSpeed = GameObject.FindObjectOfType<scriptChangeSpeed>();
    }

    void Start()
    {
        btnStart.onClick.AddListener(FonkBtnStart);
        VPIndex = VideoIndex.GetComponent<VideoPlayer>();
        checkIndexVid = true;
        enterOnlyOnce = true;
        touchBut.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (checkIndexVid)
        {
            if (!VPIndex.isPlaying)
            {
                checkIndexVid = false;
                Destroy(VideoIndex);
                GameScene.SetActive(true);
                btnStart.gameObject.SetActive(true);
            }
        }
    }

    public void FonkBtnStart()
    {
        if (enterOnlyOnce)
        {
            Destroy(EnemyA.GetComponent("DamageController2") as MonoBehaviour);
            Destroy(EnemyB.GetComponent("DamageController2") as MonoBehaviour);
            Destroy(EnemyC.GetComponent("DamageController2") as MonoBehaviour);

            LeanTween.cancelAll();

            EnemyA.AddComponent<DamageController2>();
            EnemyB.AddComponent<DamageController2>();
            EnemyC.AddComponent<DamageController2>();

            enterOnlyOnce = false;
        }
         touchBut.SetActive(true);
         Invoke("activeEnemyA",3);
        //InvokeRepeating("touchButInvoke", 0, 0.5f);
         touchButInvoke();
         EnemyA.SetActive(false);   
         btnStart.gameObject.SetActive(false);
         GameScene.transform.GetChild(1).gameObject.SetActive(true); 
         GameScene.transform.GetChild(1).transform.GetChild(0).gameObject.SetActive(true); 

        _scriptEnemy.EnemyA.transform.localPosition = new Vector3(2.8f, _scriptEnemy.EnemyA.transform.localPosition.y,0);
        _scriptEnemy.EnemyB.transform.localPosition = new Vector3(7f, _scriptEnemy.EnemyA.transform.localPosition.y,0);
        _scriptEnemy.EnemyC.transform.localPosition = new Vector3(11.2f, _scriptEnemy.EnemyA.transform.localPosition.y,0);

        _scriptBackground2.FonkSetSpeedBg(4);
        _scriptEnemy.FonkSetNewSpeed(4);
        _scriptKarakter.FonkSetSpeedPlayer(-400);

        //StartCoroutine(_scriptChangeSpeed.IncSpeed());

        imgScore.gameObject.SetActive(true);

        _scriptPlayPause.btnPlay.gameObject.SetActive(false);
        _scriptPlayPause.btnPause.gameObject.SetActive(true);
    }

    void touchButInvoke()
    {
        touchBut.transform.localScale = defaultTouchTextScale;
        LeanTween.scale(touchBut,new Vector3(1.3f, 1.3f, 1.3f),0.5f).setLoopPingPong();
        Invoke("deleteTouch",3);
    }

    public void deleteTouch()
    {
        touchBut.SetActive(false);
        CancelInvoke("deleteTouch");
    }

    void  activeEnemyA()
  {
        EnemyA.SetActive(true);
  }

}
