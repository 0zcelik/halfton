﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LitJson;
public class ScriptLogin : MonoBehaviour
{


    public InputField userName, userPass;
    public Toggle check;
    public GameObject loginButton;

    List<UserGetSet> userList = new List<UserGetSet>();
    int toggleCounter;
    JsonData jsonParser;
    int loginCounter;
    public Text notUserText;
    bool checkControl;

    public static ScriptLogin instance;
    private void Awake()
    {
        //PlayerPrefs.DeleteAll();
        instance = this;
    }
    private void Start()
    {
        checkControl = false;
        getDatas();
        // PlayerPrefs.DeleteAll();
        if (PlayerPrefs.GetString("userName").Length > 0)
        {

           /* getDatas();

            for (int i = 0; i < userList.Count; i++)
            {
                if(userList[i].getKulAd().Equals(PlayerPrefs.GetString("userName")))
                {
                    PlayerPrefs.SetInt("BestScore",userList[i].getKulPuan());
                    break;
                }
            }*/

            LoginScript.instance.b_leaderBoard = true;
            loginButton.SetActive(false);


        }
    }



    public void doYouMember()
    {
        if (check.isOn == true) //üye
        {
            Debug.Log("aktif");
            checkControl = true;
        }
         else //üye değil
        {
            Debug.Log("pasif");
            checkControl = false;
        }
    }


    public void processDB()
    {
        Debug.Log(checkControl);

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            notUserText.text = "İnternete Bağlanın";
            notUserText.GetComponent<Animation>().Play();
        }
        else
        {
            if (checkControl == true)
            {
                for (int i = 0; i < userList.Count; i++)
                {

                    Debug.Log(userList[i].getKulAd());

                    if (userList[i].getKulAd().Equals(userName.text) && userList[i].getKulSifre().Equals(userPass.text) && userList.Count > 1)
                    {
                        PlayerPrefs.SetString("userName", userName.text);
                        PlayerPrefs.SetInt("userLogin", 1);

                        if(PlayerPrefs.GetInt("BestScore") < userList[i].getKulPuan())
                        {
                            PlayerPrefs.SetInt("BestScore", userList[i].getKulPuan());
                            BringGameOverMenu.instance.txtBest.text = userList[i].getKulPuan().ToString();

                        }

                        loginCounter++;
                        notUserText.text = "Giriş Başarılı";
                        Debug.Log("Giriş Başarılı");

                        LoginScript.instance.closeLoginPanel();

//                        GameObject.Find("PanelLogin").SetActive(false);
                        //this.gameObject.SetActive(false);
                        break;
                    }
                }

                if (loginCounter == 0)
                {
                    notUserText.text = "Böyle Bir Kullanıcı Yok";
                    Debug.Log("GBöyle Bir Kullanıcı Yok");

                }
            }


            if (checkControl == false)
            {
                loginCounter = 0;
                if (userName.text.Length > 0 && userPass.text.Length > 0)
                {
                    for (int i = 0; i < userList.Count; i++)
                    {
                        if (userList[i].getKulAd().Equals(userName.text))
                        {
                            loginCounter++;
                            notUserText.text = "Bu Kullanıcı Var";
                            Debug.Log("Bu Kullanıcı Var");

                            break;
                        }
                    }

                }

                else
                {
                    notUserText.text = "Boş Alanları Doldurun";
                    Debug.Log("Boş Alanları Doldurun");

                }

                if (userName.text.Length > 0 && userPass.text.Length > 0)
                {

                    if (loginCounter == 0)
                    {
                        sendUserData();
                        notUserText.text = "Kayıt Tamamlandı";
                        Debug.Log("Kayıt Tamamlandı");

                        LoginScript.instance.closeLoginPanel();
                        PlayerPrefs.SetInt("userLogin", 1);
                        PlayerPrefs.SetString("userName", userName.text);
//                        GameObject.Find("PanelLogin").SetActive(false);

                        //this.gameObject.SetActive(false);
                    }
                }
            }

        }

    }

    public void getDatas()
    {
        StartCoroutine(getUserData());
    }

    IEnumerator getUserData()
    {
        WWWForm fakeRequest = new WWWForm();
        fakeRequest.AddField("", "");
        WWW getData = new WWW("http://ibrahimozcelik.net/Halfton/getData.php", fakeRequest);
        yield return getData;
        jsonParser = JsonMapper.ToObject(getData.text);
        for (int i = 0; i < jsonParser["users"].Count; i++)
        {
            userList.Add(new UserGetSet(jsonParser["users"][i]["kulAd"].ToString(), jsonParser["users"][i]["kulSifre"].ToString(),
            int.Parse(jsonParser["users"][i]["kulPuan"].ToString())));
        }
    }


    public void sendUserData()
    {
        StartCoroutine(saveUserData());
    }

    IEnumerator saveUserData()
    {

        WWWForm form = new WWWForm();
        form.AddField("userName", userName.text);
        form.AddField("userPass", userPass.text);
        form.AddField("userScore", 0);
        WWW sendData = new WWW("http://ibrahimozcelik.net/Halfton/login.php", form);
        PlayerPrefs.SetInt("userLogin", 1);
        yield return sendData;
    }
}



public class UserGetSet
{


    string kulAd, kulSifre;
    int kulPuan;
    public UserGetSet(string kulAd, string kulSifre, int puan)
    {
        this.kulAd = kulAd;
        this.kulSifre = kulSifre;
        this.kulPuan = puan;
    }


    public string getKulAd()
    {
        return kulAd;
    }

    public string getKulSifre()
    {

        return kulSifre;
    }

    public int getKulPuan()
    {

        return kulPuan;
    }
}
