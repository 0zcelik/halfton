﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptKarakter : MonoBehaviour
{ 
    public GameObject myPlayer;
    public float speedPlayer;

    void Start()
    {
        speedPlayer = -400;    
    }

    // Update is called once per frame
    void Update()
    {
        myPlayer.transform.Rotate(0,0,Time.deltaTime*speedPlayer);
    }
    public void FonkSetSpeedPlayer(float speed)
    {
        speedPlayer = speed;
    }
}
