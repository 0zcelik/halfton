﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageController : MonoBehaviour
{
    float pointStart, pointTop, pointBottom;
    bool enterOnlyOnce, enterOnlyOnce2;
    float Damage;

    ScriptIntro _scriptIntro;

    private void Awake()
    {
        _scriptIntro = GameObject.FindObjectOfType<ScriptIntro>();
    }

    private void Start()
    {
        pointTop = 4.5f; //3.5f;
        pointBottom = 1.55f;
        Damage = 1f; //0.6f;

        enterOnlyOnce = true;
        enterOnlyOnce2 = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (enterOnlyOnce2)
        {
            pointStart = Random.Range(pointBottom, pointTop);

            transform.localPosition = new Vector3(transform.localPosition.x, pointStart, 0);

            if (Mathf.Abs(pointTop - pointStart) < Mathf.Abs(pointBottom - pointStart)) // Yukarı yakınsa
            {
                LeanTween.moveLocalY(transform.gameObject, pointTop, zamanHesapla(pointStart));
            }
            else
            {
                LeanTween.moveLocalY(transform.gameObject, pointBottom, zamanHesapla(pointStart));
            }
            enterOnlyOnce2 = false;
        }
        if (enterOnlyOnce)
        {
            if (transform.localPosition.y == pointTop)
            {
                LeanTween.moveLocalY(transform.gameObject, pointBottom, Damage).setLoopPingPong();
                enterOnlyOnce = false;
            }
            else if (transform.localPosition.y == pointBottom)
            {
                LeanTween.moveLocalY(transform.gameObject, pointTop, Damage).setLoopPingPong();
                enterOnlyOnce = false;
            }
        }
    }

    public float zamanHesapla(float myPosition)
    {
        float zaman = Damage - (Damage - 0) * (pointTop - myPosition) / (pointTop - pointBottom);
        //Debug.Log(transform.name+" "+zaman);
        return zaman;
    }

    public void FonkBoolValue()
    {
        enterOnlyOnce = true;
        enterOnlyOnce2 = true;
    }

    IEnumerator changeSpeedEnemy()
    {
        float counter = 0;
        while (counter < 4)
        {
            yield return new WaitForSeconds(1);

            counter += 1;
            Damage -=.1f;
        }
    }
}
