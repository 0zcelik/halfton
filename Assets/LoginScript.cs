﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LoginScript : MonoBehaviour
{
    public GameObject InputUser,InputPassword,pnlGroup,btnLogin,imgLeaderBoard;
    public TextMeshProUGUI txtLoginSend;
    public Text txtUser,txtPass;
    float vectUserFar, vectPassFar, vectGroupFar;
    float vectUserNear, vectPassNear, vectGroupNear;
    public static LoginScript instance;
    public    bool login, send, b_user, b_pass, b_v1, b_v2, b_v3, b_leaderBoard,b_LB;

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        vectUserFar = 163.8f;
        vectPassFar = 168;
        vectGroupFar = 167;

        vectUserNear = -0.2f;
        vectPassNear = 0;
        vectGroupNear = 0;

        login = true;
        send = false;

        b_pass = false;
        b_user = false;

        b_v1 = true;
        b_v2 = true;
        b_v3 = true;

        

        b_LB = true;

        txtUser.text = "";
        txtPass.text = "";

        Debug.Log("InputUser: "+ InputUser.transform.localPosition);
        Debug.Log("InputPassword: "+InputPassword.transform.localPosition);
        Debug.Log("pnlGroup: "+pnlGroup.transform.localPosition);
    }
    void Update()
    {
        //Debug.Log(imgLeaderBoard.transform.localPosition);
    }

    public void FonkBtnLogin() {
        if (login) {
            StartCoroutine(showLoginMenu());
            login = false;
            send = true;
            txtLoginSend.text = "send";
        }
        else if (send)
        {
            // fonksiyona ilk girişte gimez
            if (txtUser.text.Equals("")) 
            {
                //titre
                Debug.Log("UserTitreşim");
                if (b_v1) { LeanTween.moveLocalX(InputUser,30 ,.1f).setLoopPingPong().setLoopCount(3); b_v1 = !b_v1; }
                else { LeanTween.moveLocalX(InputUser,0 ,.1f).setLoopPingPong().setLoopCount(3); b_v1 = !b_v1; }
                b_user = false;
            }
            else
            {
                Debug.Log("İsim Dolu Geliyor");
                b_user = true;
            }
            if (txtPass.text.Equals(""))
            {
                //titre
                Debug.Log("PassTitreşim");
                if (b_v2) { LeanTween.moveLocalX(InputPassword, 30, .1f).setLoopPingPong().setLoopCount(3); b_v2 = !b_v2; }
                else { LeanTween.moveLocalX(InputPassword, 0, .1f).setLoopPingPong().setLoopCount(3); b_v2 = !b_v2; }
                
                b_pass = false;
            }
            else
            {
                Debug.Log("Pass Dolu geliyor");
                b_pass = true;
            }

            if (b_user&&b_pass) 
            {
                ScriptLogin.instance.processDB();
                // herşeyi yok et
            }
        }
    }

    public void closeLoginPanel()
    {
        btnLogin.SetActive(false);
        InputUser.SetActive(false);
        InputPassword.SetActive(false);
        pnlGroup.SetActive(false);
        b_leaderBoard = true;
        Debug.Log("Herşeyy yolunda");

    }

    public void FonkBtnLeaderBoard() {
        if (!b_leaderBoard)
        {
            if (b_v3) { LeanTween.moveLocalX(btnLogin, 30, .1f).setLoopPingPong().setLoopCount(3); b_v3 = !b_v3; }
            else { LeanTween.moveLocalX(btnLogin, 0, .1f).setLoopPingPong().setLoopCount(3); b_v3 = !b_v3; }
        }
        else
        {
            if (b_LB)
            {
                LeanTween.moveLocalY(imgLeaderBoard, 0, .5f); b_LB = !b_LB;
                ScriptLeaderBoard.instance.scoreTableEnable();    
            }
            else{
                LeanTween.moveLocalY(imgLeaderBoard, 1015, .5f); b_LB = !b_LB;

            }
        }

    }

    IEnumerator showLoginMenu()
    {
        LeanTween.moveLocalY(pnlGroup, vectGroupNear, 0.5f);
        yield return new WaitForSeconds(.5f);
        LeanTween.moveLocalY(InputUser, vectUserNear, 0.5f);
        yield return new WaitForSeconds(.5f);
        LeanTween.moveLocalY(InputPassword, vectPassNear, 0.5f);
    }
}
