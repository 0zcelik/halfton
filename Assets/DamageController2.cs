﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageController2 : MonoBehaviour
{
    float pointStart, pointTop, pointBottom;
    float damageTime;
    bool EOO;
    bool TopBottom;
    int counter;

    int LID;
    void Start()
    {
        pointBottom = 1.55f;//1.55
        pointTop = 3.9f;//4.05 / 3.85
        damageTime = 1.6f/*0.6f*/;
        
        FonkSetPointStart(pointStart);
        
        EOO = true;
        counter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        damageTime = Random.RandomRange(0.8f,1.2f);
//        Debug.Log("DAMAGE TIME " + damageTime);
        if (EOO)
        {
            if (TopBottom == true) // Yukarı 
            {
                if (transform.localPosition.y == pointTop)
                {
                    LID = LeanTween.moveLocalY(transform.gameObject,pointBottom,damageTime).setLoopPingPong().id;
                    EOO = false;
                }
            }
            else if (TopBottom == false) // Aşşağı
            {
                if (transform.localPosition.y == pointBottom)
                {
                    LID = LeanTween.moveLocalY(transform.gameObject, pointTop, damageTime).setLoopPingPong().id;
                    EOO = false;
                }
            }
        }
        if (counter<3)
        {
            if (TopBottom)
            {
                if (transform.localPosition.y == pointBottom)
                {
                    counter++; damageTime -= 0.2f;
                    //Debug.Log(counter);
                    LeanTween.cancel(LID);
                    LID = LeanTween.moveLocalY(transform.gameObject, pointTop, damageTime).setLoopPingPong().id;
                }
            }
            else
            {
                if (transform.localPosition.y == pointTop)
                {
                    counter++; damageTime -= 0.2f;
                    //Debug.Log(counter);
                    LeanTween.cancel(LID);
                    LID = LeanTween.moveLocalY(transform.gameObject, pointBottom, damageTime).setLoopPingPong().id;
                }
            }
           // Debug.Log("DamageTime: "+damageTime);
        }
    }

    void FonkSetPointStart(float pointStart)
    {
        pointStart = Random.Range(pointBottom, pointTop);
        transform.localPosition = new Vector3(transform.localPosition.x, pointStart, 0);

        if (Mathf.Abs(pointStart - pointTop)< Mathf.Abs(pointStart - pointBottom))
        {
            float newTime = damageTime / Mathf.Abs(pointBottom-pointTop) * Mathf.Abs(pointTop - pointStart);
            TopBottom = true;
            LeanTween.moveLocalY(transform.gameObject, pointTop, newTime);
        }
        else
        {
            float newTime = damageTime / Mathf.Abs(pointBottom - pointTop) * Mathf.Abs(pointBottom - pointStart);
            TopBottom = false;
            LeanTween.moveLocalY(transform.gameObject, pointBottom, newTime);
        }
    }
}
