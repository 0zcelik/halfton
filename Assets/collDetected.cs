﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collDetected : MonoBehaviour
{
    ScriptBackground2 _scriptBackground2;
    ScriptEnemy _scriptEnemy;
    ScriptKarakter _scriptKarakter;
    BringGameOverMenu _bringGameOverMenu;
    ScriptSpeed _scriptSpeed;

    public GameObject CodeManagers;

   

    private void Awake()
    {
        _scriptSpeed = GameObject.FindObjectOfType<ScriptSpeed>();
        _bringGameOverMenu = GameObject.FindObjectOfType<BringGameOverMenu>();
        _scriptBackground2 = GameObject.FindObjectOfType<ScriptBackground2>();
        _scriptEnemy = GameObject.FindObjectOfType<ScriptEnemy>();
        _scriptKarakter = GameObject.FindObjectOfType<ScriptKarakter>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.Equals("EnemyA"))
        {
            stopBackgroundEnemyRotate();
            disperse();
        }
        if (collision.gameObject.name.Equals("EnemyB"))
        {
            stopBackgroundEnemyRotate();
            disperse();
        }
        if (collision.gameObject.name.Equals("EnemyC"))
        {
            stopBackgroundEnemyRotate();
            disperse();
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        _bringGameOverMenu.SetCurrentScore();
    }

    void stopBackgroundEnemyRotate()
    {
        //Dokunarak Input Vermesini engelliyoruz 
        //çünkü Aşşağıdaki kodu ezer 
        //ve tekrar herşey sola doğru harekete geçer 
        _scriptSpeed.FonkSetTouch(false);

        // Tüm Hareketi durdurduk
        _scriptBackground2.FonkSetSpeedBg(0);
        _scriptEnemy.FonkSetNewSpeed(0);
        _scriptKarakter.FonkSetSpeedPlayer(0);

        // Random Yukarı Aşşağıyı Durdurduk
        LeanTween.cancelAll();

        // Çarpışmadan Sonra Yukarı Doğru Bi Haraket Verdik
        LeanTween.moveLocalY(_scriptEnemy.EnemyA, 3.5f, 1.2f);
        LeanTween.moveLocalY(_scriptEnemy.EnemyB, 3.5f, 1.2f);
        LeanTween.moveLocalY(_scriptEnemy.EnemyC, 3.5f, 1.2f);
    }

    void disperse()
    {
        _bringGameOverMenu.FonkSetGameValue(true);
        transform.gameObject.SetActive(false);
        
        for (int i = 0; i < 10; i++)
        {
            GameObject newPlayer = Instantiate(transform.gameObject);
            newPlayer.SetActive(true);

            Destroy(newPlayer.GetComponent("collisonDetected") as MonoBehaviour);
            Destroy(newPlayer.GetComponent<CircleCollider2D>());
            Destroy(newPlayer.GetComponent<Rigidbody2D>());

            newPlayer.transform.parent = transform.gameObject.transform.parent;
            newPlayer.transform.localPosition = transform.gameObject.transform.localPosition;
            // float div = Random.Range(3, 6);
            float div = Random.Range(1.5f, 3.5f);
            float myScale = transform.gameObject.transform.localScale.x / div;
            newPlayer.transform.localScale = new Vector3(myScale, myScale, myScale);


            if (Random.Range(0, 2) % 2 == 0) { LeanTween.moveLocalX(newPlayer, 7, Random.Range(1, 6)); }
            else { LeanTween.moveLocalX(newPlayer, -7, Random.Range(1, 6)); }

            if (Random.Range(0, 2) % 2 == 0) { LeanTween.moveLocalY(newPlayer, 7, Random.Range(1, 6)); }
            else { LeanTween.moveLocalY(newPlayer, -7, Random.Range(1, 6)); }

            LeanTween.rotateZ(newPlayer, 180, .2f).setLoopPingPong().setRepeat(5);
            Destroy(newPlayer, 1);
        }
    }
}
