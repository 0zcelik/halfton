﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScriptPlayAgain : MonoBehaviour
{
    public GameObject EnemyA, EnemyB, EnemyC;
    public Button btnPlayAgain,btnScip;
    public GameObject PanelMenu,PanelGameOver,PanelFace;

    public static ScriptPlayAgain instance;

    ScriptIntro _scriptIntro;
    ScriptSpeed _scriptSpeed;
    BringGameOverMenu _bringGameOverMenu;

    private void Awake()
    {
        instance = this;
        _bringGameOverMenu = GameObject.FindObjectOfType<BringGameOverMenu>();
        _scriptSpeed = GameObject.FindObjectOfType<ScriptSpeed>();
        _scriptIntro = GameObject.FindObjectOfType<ScriptIntro>();
    }

    void Start()
    {
        btnScip.onClick.AddListener(FonkBtnSkip);
        btnPlayAgain.onClick.AddListener(FonkBtnPlayAgain);
    }

    void FonkBtnPlayAgain()
    {
        Debug.Log("Again Basıldı!");
        PanelFace.SetActive(true);
    }

  public  void FonkBtnSkip()
    {
        Debug.Log("Scip Basıldı!");
        PanelFace.SetActive(false);
        PanelMenu.SetActive(false);
        //
        _bringGameOverMenu.SetCurrentScore(false);
        _bringGameOverMenu.imgScore.gameObject.SetActive(true);
        
        //
        _scriptSpeed.FonkSetTouch(true);
        EnemyA.AddComponent<DamageController2>();
        EnemyB.AddComponent<DamageController2>();
        EnemyC.AddComponent<DamageController2>();
        //     transform.gameObject.AddComponent<ScriptSpeed>();
        _scriptIntro.FonkBtnStart();
        
    }
}
