﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class BringGameOverMenu : MonoBehaviour
{
    bool GameOver;
    public AudioSource deadSound;
    public GameObject FolderGameOverMenu;
    public GameObject EnemyA, EnemyB, EnemyC;
    public Image imgScore; 
    public TextMeshProUGUI txtBest,txtCurrent;
    public static BringGameOverMenu instance;
    public GameObject adwordButton,playButton,referancePoint;
    int BestScore,currentScore;
    collDetected _collDetected;
    public int resumeCounter;
    public bool adwordResumeControl;
    private void Awake()
    {
        instance = this;
        _collDetected = GameObject.FindObjectOfType<collDetected>();
    }
    Vector3 adwordDefaultButton;
    void Start()
    {
        
        adwordDefaultButton = playButton.transform.localPosition;

        adwordResumeControl = true;
        //PlayerPrefs.DeleteAll();
        GameOver = false;
        currentScore = 0;
        if (PlayerPrefs.HasKey("BestScore"))
        {
            BestScore = PlayerPrefs.GetInt("BestScore");
        }
        else
        {
            BestScore = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {



        //***************************************************************

        if(Input.GetKeyDown(KeyCode.O))
        {
            currentScore += 10000;
        }
        //***************************************************************



        if (GameOver)
        {
            

            Debug.Log("GAME OVER");
            deadSound.Play();
            if (resumeCounter == 0) 
            {
                adwordButton.SetActive(true);
                playButton.transform.localPosition = adwordDefaultButton;
            }
            else
            {
                playButton.transform.localPosition = referancePoint.transform.localPosition;
                adwordButton.SetActive(false);
                resumeCounter = 0;
            }


          
            /*else if(resumeCounter % 2 == 1 && adwordButton.gameObject.activeInHierarchy)
            {
                adwordButton.SetActive(false);
            }
            else
            {
                adwordButton.SetActive(true);
            }*/


            //
            //panelSettings.transform.SetSiblingIndex(1);
            //
            //EnterOnlyOnce = false;
            GameOver = false;
            //
            BestScore = PlayerPrefs.GetInt("BestScore");
            Debug.Log("BEST SCORE " + BestScore);
            Debug.Log("CURRENT SCORE " + currentScore);

            if (currentScore > BestScore)
            {
                BestScore = currentScore;
                PlayerPrefs.SetInt("BestScore", BestScore);
                ScriptUpdateScore.instance.updateScore();

                txtBest.text = BestScore.ToString();
                txtCurrent.text = BestScore.ToString();
            }
            else
            {
                txtBest.text = BestScore.ToString();
                txtCurrent.text = currentScore.ToString();
            }
            //
            StartCoroutine(bringGameOverMenu());
        }
       
    }

    public void FonkSetGameValue(bool GameValue)
    {
        GameOver = GameValue;
    }
    IEnumerator bringGameOverMenu()
    {
        yield return new WaitForSeconds(1);
        FolderGameOverMenu.SetActive(true);


        Destroy(EnemyA.GetComponent("DamageController2") as MonoBehaviour);
        Destroy(EnemyB.GetComponent("DamageController2") as MonoBehaviour);
        Destroy(EnemyC.GetComponent("DamageController2") as MonoBehaviour);
        imgScore.gameObject.SetActive(false);
    }

    public void setBestScore(int NewBestscore)
    {
        PlayerPrefs.SetInt("BestScore", NewBestscore);
    }

    

    public void SetCurrentScore(bool state = true)
    {
        if (state)
        {
            currentScore++;
        }
        else
        {
            if(adwordResumeControl)
            {
                currentScore = 0;       
            }
            else
            {
                adwordResumeControl = true;
               
            }
        }
        imgScore.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = currentScore.ToString();
    }

    public GameObject loginPanel;
    public void openLoginPanel()
    {
        loginPanel.SetActive(true);
    }

    public void closeLoginPanel()
    {
        loginPanel.SetActive(false);
    }
}
